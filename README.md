# Continuous Deployment with Spinnaker

### Pipeline
- `demo-prod` is the production Service
- A Kubernetes Deployment, `demo-v0-0-1`, is running app version `0.0.1`. Optionally a Service `demo-v0-0-1` is also created.
- Client requests to `demo-prod` Service are routed the pods running app version `0.0.1`
- When a new version, `0.0.2`, is available, a new Kubernetes Deployment, `demo-v0-0-2`, is created that runs app version `0.0.2` without affecting `v0.0.1` app. This new version of app can be accessed via `demo-v0-0-2` Service
- Once we're satisfied with version `0.0.2`, we can go ahead and **approve** *Manual Judgment* stage.
- Approval of *Manual Judgment* stage then triggers `demo-prod` *Patch* stage which patches `demo-prod` Service's selector so that it now targets `v0.0.2` pods instead of `v0.0.1`.

![pipeline](./images/spinnaker-cd.svg)


### Spinnaker Installation

Halyard is a command-line administration tool that manages the lifecycle of your Spinnaker deployment, including writing & validating your deployment’s configuration, deploying each of Spinnaker’s microservices, and updating the deployment.
First [install Halyard](https://www.spinnaker.io/setup/install/halyard/).

Enable Kubernetes provider
```
hal config provider kubernetes enable
```

Enable artifact support
```
hal config features edit --artifacts true
```

Add an account, say k8s-dev, to the Kubernetes provider. The account is basically kubeconfig context.
```
hal config provider kubernetes account add k8s-dev \
  --context $(kubectl config current-context) \
  --provider-version v2
```

Specify account to use for deployment of Spinnaker itself
```
hal config deploy edit --type distributed --account-name k8s-dev
```

Select Spinnaker version
```
hal config version edit --version $(hal version latest -q)
```

Install MinIO S3 compatible storage with helm v3 in spinnaker namespace.
```
kubectl create namespace spinnaker
helm install minio --namespace spinnaker \
  --set accessKey="insecureaccesskey" \
  --set secretKey="insecuresecretkey" \
  stable/minio
```

Configure Spinnaker to use MinIO storage
```
mkdir -p ~/.hal/default/profiles
echo "spinnaker.s3.versioning: false" > ~/.hal/default/profiles/front50-local.yml
hal config storage s3 edit \
  --endpoint http://minio:9000 \
  --access-key-id "insecureaccesskey" \
  --secret-access-key "insecuresecretkey" \
  --path-style-access true
hal config storage edit --type s3
```

#### Integrations
Concourse
```
hal config ci concourse enable
hal config ci concourse master add mymaster \
  --url "http://concourse-web.concourse:8080" --username test --password test
hal config ci concourse master edit mymaster \
  --add-read-permission  --add-write-permission
```

Helm
```
hal config artifact helm enable
hal config artifact helm account add stable \
  --repository https://kubernetes-charts.storage.googleapis.com
hal config artifact helm account add chartmuseum \
  --repository http://chartmuseum-chartmuseum.chartmuseum:8080
```

Chartmuseum chart repo if needed
```
# helm v3
kubectl create ns chartmuseum
helm install chartmuseum -n chartmuseum --set env.open.DISABLE_API=false stable/chartmuseum
kubectl -n chartmuseum port-forward svc/chartmuseum-chartmuseum 8080
helm repo add chartmuseum http://127.0.0.1:8080
helm plugin install https://github.com/chartmuseum/helm-push
helm create mychart
helm push ./mychart chartmuseum
helm repo update
helm install mychart chartmuseum/mychart
```

Gitlab
```
hal config artifact gitlab enable
echo -n my-gitlab-personal-access-token > gitlab-token
hal config artifact gitlab account add mygitlabaccount --token-file=gitlab-token
```

Deploy Spinnaker
```
hal deploy apply
```

Verify installation. The installation may take 4-5 minutes or more depending on network speed and hardware.
```
kubectl --namespace spinnaker get pods
```

Access Spinnaker web UI
```
hal deploy connect
```
Above command port-forwards spin-deck and spin-gate services on port 9000 and 8084 respectively. If everything went well, web UI should be available at http://127.0.1:9000.

Cleanup
```
hal deploy clean
```